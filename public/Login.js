 import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import axios from 'axios';
import App from '../src/routes/app/components/MainApp';
var apiBaseUrl = "http://msched.us-west-2.elasticbeanstalk.com/";

class Login extends Component {
    constructor(props) {
        super(props);

        var localloginComponent = [];
        localloginComponent.push(
            <MuiThemeProvider>
                <div>
                    <TextField
                        hintText="Enter your username"
                        floatingLabelText="User Name"
                        onChange={(event, newValue) => this.setState({username: newValue})}
                    />
                    <br/>
                    <TextField
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange={(event, newValue) => this.setState({password: newValue})}
                    />
                    <br/>
                    <RaisedButton label="Submit" primary={true} style={style}
                                  onClick={(event) => this.handleClick(event)}/>
                </div>
            </MuiThemeProvider>
        )

        this.state = {
            username: '',
            password: '',
            menuValue: 1,
            loginComponent: localloginComponent,
            loginRole: 'pro'
        }
    }

    componentWillMount() {
        var localloginComponent = [];
        localloginComponent.push(
            <MuiThemeProvider>
                <div>
                    <TextField
                        hintText="Enter your username"
                        floatingLabelText="User Name"
                        onChange={(event, newValue) => this.setState({username: newValue})}
                    />
                    <br/>
                    <TextField
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange={(event, newValue) => this.setState({password: newValue})}
                    />
                    <br/>
                    <RaisedButton label="Submit" primary={true} style={style}
                                  onClick={(event) => this.handleClick(event)}/>
                </div>
            </MuiThemeProvider>
        )
        this.setState({menuValue: 1, loginComponent: localloginComponent, loginRole: 'student'});
    }

    handleClick(event) {
        var self = this;
        var payload = {
            "identifier": this.state.username,
            "password": this.state.password
        }

        console.info("Attempting to login " + this.state.username);

        axios.get(apiBaseUrl + '/Login?identifier=' + payload.identifier + '&password=' + payload.password)
            .then(function (response) {
                console.log(response);
                if (response.status === 200) {
                    console.log("Login successful");
                    var uploadScreen = [];
                    uploadScreen.push(<App/>);
                    self.props.appContext.setState({loginPage: [], uploadScreen: uploadScreen});
                } else if (response.status === 204) {
                    console.log("Username password do not match");
                    alert(response.data.success);
                } else {
                    console.log("Username does not exists");
                    alert("Username does not exist");
                }
        }).catch(function (error) {
            console.log(error);
        });
    }

    handleMenuChange(value) {
        console.log("menuvalue", value);
        var loginRole;

        var localloginComponent = [];
        loginRole = 'student';
        localloginComponent.push(
            <MuiThemeProvider>
                <div>
                    <TextField
                        hintText="Enter your username"
                        floatingLabelText="User Name"
                        onChange={(event, newValue) => this.setState({username: newValue})}
                    />
                    <br/>
                    <TextField
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange={(event, newValue) => this.setState({password: newValue})}
                    />
                    <br/>
                    <RaisedButton label="Submit" primary={true} style={style}
                                  onClick={(event) => this.handleClick(event)}/>
                </div>
            </MuiThemeProvider>
        )
        this.setState({
            menuValue: value,
            loginComponent: localloginComponent,
            loginRole: loginRole
        })
    }

    render() {
        return (
            <div>
                <MuiThemeProvider>
                    <AppBar
                        title="Login"
                    />
                </MuiThemeProvider>
                {this.state.loginComponent}
            </div>
        );
    }
}

const style = {
    margin: 15,
};

export default Login;